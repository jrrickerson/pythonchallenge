import re
import requests


start_token = "12345"
url_template = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing={token}"


def get_next_token(template, token, pattern):
    response = requests.get(template.format(token=token))
    print(response.text)
    match = pattern.search(response.text)
    if match:
        next_token = match.group(0)
        get_next_token(template, next_token, pattern)
    elif re.match("Yes. Divide by two and keep going.", response.text):
        next_token = str(int(token) // 2)
        get_next_token(template, next_token, pattern)
    else:
        return response.text


def main(template, token):
    token_pattern = re.compile(r"(?<=and the next nothing is )[\d]+$")
    return get_next_token(template, token, token_pattern)


if __name__ == "__main__":
    print("Solving Level 4:", "http://www.pythonchallenge.com/pc/def/linkedlist.html")
    print(main(url_template, start_token))


