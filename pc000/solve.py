

def main():
    return 2 ** 38


if __name__ == "__main__":
    print("Solving Level 0:", "http://www.pythonchallenge.com/pc/def/0.html")
    print(main())
