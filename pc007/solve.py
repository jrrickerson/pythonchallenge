import io
import requests
from PIL import Image

IMAGE_URL = "http://www.pythonchallenge.com/pc/def/oxygen.png"


def load_image_data(file_url):
    r = requests.get(file_url)
    data = io.BytesIO(r.content)
    return Image.open(data)


def find_grayscale_line(image):
    width, height = image.size
    grayscale = []
    pixels = image.getdata()
    for row_num in range(height):
        row_start = row_num * width
        pixel = pixels[row_start]
        if pixel[0] == pixel[1] == pixel[2] and pixel[3] == 255:
            print(f"RGB all match at line 0,{row_num}")
            grayscale = [
                pixels[p] for p in range(row_start, row_start + width)
                if pixels[p][0] == pixels[p][1] == pixels[p][2]]
            break
    return grayscale


def deduplicate_pixels(pixels, skip_duplicates=1):
    return [pixels[i] for i in range(0, len(pixels), skip_duplicates)]


def rgb_to_ascii(pixels):
    characters = [chr(p[0]) for p in pixels if p[0] > 31 and p[0] < 127]
    return ''.join(characters)


def extract_list(text):
    s = text[text.find("["):]
    return eval(s)


def main(url):
    image = load_image_data(url)
    grayscale_data = find_grayscale_line(image)
    pixels = deduplicate_pixels(grayscale_data, skip_duplicates=7)
    message = rgb_to_ascii(pixels)
    print(message)
    code_list = extract_list(message)
    solution = ''.join([chr(c) for c in code_list])
    return solution


if __name__ == "__main__":
    print(
        "Solving Level 7:",
        "http://www.pythonchallenge.com/pc/def/oxygen.html")
    print(main(IMAGE_URL))
