import pickle
import requests


data_url = "http://www.pythonchallenge.com/pc/def/banner.p"

def generate_spans(runlength_span_list):
    """Function to generate string from a list of run-length encoded tuples.
       Recursive just because."""
    if runlength_span_list:
        span = runlength_span_list[0]
        return span[0] * span[1] + generate_spans(runlength_span_list[1:])
    return ''

def generate_lines(runlength_line_list):
    """Function to generate text lines from a list of lists
       Recursive solution for reasons."""
    if runlength_line_list:
        line = runlength_line_list[0]
        return generate_spans(line) + "\n" + generate_lines(runlength_line_list[1:])
    return ''

def main(url):
    byte_data = requests.get(url).content
    decoded = pickle.loads(byte_data)
    expanded = generate_lines(decoded)

    return expanded

if __name__ == "__main__":
    print("Solving Level 5:", "http://www.pythonchallenge.com/pc/def/peak.html")
    print(main(data_url))
