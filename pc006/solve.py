import io
import requests
import zipfile
import re

IMAGE_URL = "http://www.pythonchallenge.com/pc/def/channel.jpg"


def get_zipfile(byte_data):
    zipdata = io.BytesIO(byte_data)
    z = zipfile.ZipFile(zipdata)
    return z


def read_zipfile_file(zf, filename):
    return zf.read(filename).decode('utf8')


def next_pair(zf, pattern, filename):
    text = read_zipfile_file(zf, filename)
    comment = zf.getinfo(filename).comment
    pair = [(filename, comment)]
    match = pattern.search(text)
    if match:
        return pair + next_pair(zf, pattern, match.group(0) + ".txt")
    else:
        return pair

def main(url):
    # Change the extension
    zip_url = url[:-3] + "zip"
    response = requests.get(zip_url)
    zf = get_zipfile(response.content)
    readme_text = read_zipfile_file(zf, "readme.txt")
    first_file = re.search("start from (\d+)", readme_text).group(1) + ".txt"
    pairs = next_pair(zf, re.compile("\d+$"), first_file)
    message = ''.join([p[1].decode("utf8") for p in pairs])
    return message


if __name__ == "__main__":
    print("Solving Level 6:", "http://www.pythonchallenge.com/pc/def/channel.html")
    print(main(IMAGE_URL))
