# Level number left-padded to 3 digits
LEVEL_NAME = $(shell printf "%03g" $(LEVEL))
# Directory name for level code and data
LEVEL_DIR = pc$(LEVEL_NAME)
KEY=$(KEY)
EDITOR=vim


# Create a new directory for solving a new level
newlevel:
	mkdir -p $(LEVEL_DIR)
	jinja2 solve.py.j2 -D LEVEL=$(LEVEL) -D URL="$(URL)" > $(LEVEL_DIR)/solve.py

# Open solution script in editor
edit:
	$(EDITOR) $(LEVEL_DIR)/solve.py

# Run solution for specific level
solve:
	python3 $(LEVEL_DIR)/solve.py

# Run solution for specific level with instrumentation
timer:
	@time -f "Execution time: %es" python3 $(LEVEL_DIR)/solve.py

# Verify a solution by requesting the next URL with the provided
# key / passcode
verify:
	curl http://www.pythonchallenge.com/pc/def/$(KEY).html
