import string
from collections import deque

encoded_message = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."


def main(encoded):
    shifted_alpha = deque(string.ascii_lowercase)
    shifted_alpha.rotate(-2)
    cypher = str.maketrans(string.ascii_lowercase, ''.join(shifted_alpha))
    return encoded.translate(cypher)

if __name__ == "__main__":
    print("Solving Level 1:", "http://www.pythonchallenge.com/pc/def/map.html")
    print(main(encoded_message))
    print(main("map"))
